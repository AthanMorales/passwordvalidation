/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password;

/**
 *
 * @author Mauricio
 */
public class PasswordValidator {
    private static boolean checkLength(String password){
        return password.length( ) >= 8;
    }
    
    private static boolean checkUppercaseChars(String password){
        char letter = 'x';
        for (int i = 65; i <= 90 ; i++){
            letter=(char)i;
            if(password.contains(String.valueOf(letter))){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkSpecialChars(String password){
        String[] specialChar={"@","$","+","!","#","?","^","&"};
        for (int i = 0; i < 8; i++){
            if(password.contains(specialChar[i])){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkDigit(String password){
        for (int i = 0; i <= 9 ; i++){
            if(password.contains(String.valueOf(i))){
                return true;
            }
        }
        return false;
    }
    
    public static boolean validatePassword(String password){
        return checkLength( password )&& checkSpecialChars(password) && checkUppercaseChars(password) && checkDigit(password);
    }
}
