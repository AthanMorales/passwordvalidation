/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Athan
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /*Start Password length test-----------------------------------------------------------------------------
    *
    */
    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validatePassword length regular");
        String password = "abCd@efghi0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    
    @Test
    public void testValidatePasswordLengthBondaryIn(){
        System.out.println("validatePassword length bondary-in");
        String password = "abCde#f0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBondaryOut(){
        System.out.println("validatePassword length bondary-out");
        String password = "abC@de0";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    /*
    *
    *///ends password length test---------------------------------------------------------------
    
    
    /*Start At least one special character test-----------------------------------------------------------------------------
    *
    */
    @Test
    public void testValidatePasswordSpecialChars(){
        System.out.println("validatePassword SpecialChars Regular");
        String password = "abCde?fg&0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordSpecialCharsException(){
        System.out.println("validatePassword SpecialChars Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordSpecialCharsBondaryIn(){
        System.out.println("validatePassword SpecialChars Bondary-In");
        String password = "$abCdefgh0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordSpecialCharsBondaryOut(){
        System.out.println("validatePassword SpecialChars Bondary-Out");
        String password = "abCdefgh0j";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    /*
    *
    *///ends least one special character test---------------------------------------------------------------
    
    
    
    /*Start At least one uppercase character test-----------------------------------------------------------------------------
    *
    */
    @Test
    public void testValidatePasswordUppercaseChars(){
        System.out.println("validatePassword Uppercase Regular");
        String password = "abCde?fgh0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordUppercaseCharsException(){
        System.out.println("validatePassword Uppercase Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordUppercaseCharsBondaryIn(){
        System.out.println("validatePassword Uppercase Bondary-In");
        String password = "abCde?fgh0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordUppercaseCharsBondaryOut(){
        System.out.println("validatePassword Uppercase Bondary-Out");
        String password = "abcde?fgh0";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    /*
    *
    *///End At least one uppercase character test-----------------------------------------------------------------------------
    
    
    
    /*Start At least one digit test-----------------------------------------------------------------------------
    *
    */
    @Test
    public void testValidatePasswordDigit(){
        System.out.println("validatePassword Digit Regular");
        String password = "abCde?fgh0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordDigitException(){
        System.out.println("validatePassword Digit Exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordDigitBodaryIn(){
        System.out.println("validatePassword Digit Bondary_in");
        String password = "abCde?fgh0";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordDigitBodaryOut(){
        System.out.println("validatePassword Digit Bondary_Out");
        String password = "abCde?fghi";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password dos not meet the requirement",expResult, result);
        //fail("The test case is a prototype.");
    }
    /*
    *
    *///End At least one digit test-----------------------------------------------------------------------------
}
